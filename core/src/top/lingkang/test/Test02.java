package top.lingkang.test;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class Test02 {
    public static void main(String[] args) {
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> e) throws Exception {
                e.onNext(1);
                e.onNext(2);
                e.onNext(3);
                e.onComplete(); //结束
                e.onNext(4);
            }
        })
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        System.out.println("zhao  onSubscribe: " + d.isDisposed());
                    }

                    @Override
                    public void onNext(@NonNull Integer integer) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println("zhao onNext: " + integer);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        System.out.println("zhao onError: ");
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("zhao onComplete: ");
                    }
                });
    }
}
