package top.lingkang.test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author lingkang
 */
public class RunTest extends ApplicationAdapter {
    private Texture texture;
    private SpriteBatch batch;
    private float R = 100; // 半径
    private float originX = 200, originY = 200; // 原点
    private float Pi = 3.1416f; // π 这里我们不用双精度，3.1416够了  PI = 3.14159265358979323846 ....
    private float angle = 1f; // 旋转的角度

    @Override
    public void create() {
        batch = new SpriteBatch();
        texture = new Texture(Gdx.files.internal("plane/player1.png"));
    }

    @Override
    public void render() {
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // 清除屏幕

        //正弦函数 sinθ=y/r  角度 π不需要太过精确
        float a = 2 * Pi / 360 * angle;
        float x = originX + (float) Math.sin(a) * R;
        float y = originY + (float) Math.cos(a) * R;
        // 角度增加
        if (++angle > 360f) {
            angle = 1f;
        }

        // 绘制
        batch.begin();
        batch.draw(texture, x, y);
        batch.end();
    }
}
