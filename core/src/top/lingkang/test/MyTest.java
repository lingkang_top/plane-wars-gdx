package top.lingkang.test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import top.lingkang.sprite.BaseSprite;

public class MyTest extends ApplicationAdapter {
    private BaseSprite bullet, enemy;
    public SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        bullet = new BaseSprite();
        enemy = new BaseSprite();

        bullet.setRegion(new TextureRegion(new Texture("map1/tiles_packed.png"),
                0, 0, 16, 16));
        bullet.setX(200);
        bullet.setY(0);
        enemy.setRegion(new TextureRegion(new Texture(Gdx.files.internal("plane/ship_0005.png")), 0, 0, 32, 32));
        enemy.setX(200);
        enemy.setY(500);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // 清除屏幕

        batch.begin();
        // enemy纹理大小是 32*32
        batch.draw(enemy, enemy.getX()-50, enemy.getY());
        batch.draw(enemy, enemy.getX(), enemy.getY(),
                16, 16, // 旋转原点
                32, 32, // 绘制纹理的大小
                1, 1, // 放大缩小的 比例
                180 // 在旋转原点 旋转的角度
                    );
        batch.draw(enemy, enemy.getX()+50, enemy.getY(),
                16, 16, // 旋转原点
                32, 32, // 绘制纹理的大小
                1, 1, // 放大缩小的 比例
                45 // 在旋转原点 旋转的角度
        );
        batch.draw(enemy, enemy.getX()+100, enemy.getY(),
                16, 16, // 旋转原点
                32, 32, // 绘制纹理的大小
                1, 1, // 放大缩小的 比例
                90 // 在旋转原点 旋转的角度
        );
        batch.draw(bullet, bullet.getX(), bullet.getY());
        batch.end();
        bullet.setY(bullet.getY() + 2f);

        if (bullet.hasOverlap(enemy)){
            System.out.println(1);
        }
    }
}
