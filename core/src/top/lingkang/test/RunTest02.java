package top.lingkang.test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author lingkang
 */
public class RunTest02 extends Game {
    private Texture texture;
    private SpriteBatch batch;
    private float originX = 0, originY = 200; // 原点
    private float Pi = 3.1416f; // π 这里我们不用双精度，3.1416够了  PI = 3.14159265358979323846 ....
    private float angle = 0f; // 旋转的角度
    private float r = 100;

    @Override
    public void create() {
        batch = new SpriteBatch();
        texture = new Texture(Gdx.files.internal("plane/player1.png"));
    }

    @Override
    public void render() {
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // 清除屏幕
        if (++originX > 648) { // 坐标移动
            originX = 0;
        }
        float a = 2 * Pi / 360 * angle;
        float y = originY + (float) Math.sin(a) * r; // r 为顶峰幅度值
        // 角度增加
        if (++angle > 360f) {
            angle = 0f;
        }
        // 绘制
        batch.begin();
        batch.draw(texture, originX, y);
        batch.end();
    }
}
