package top.lingkang.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import top.lingkang.MyGdxGame;
import top.lingkang.constants.BulletType;
import top.lingkang.constants.MoveType;
import top.lingkang.constants.PlaneType;
import top.lingkang.constants.PropType;
import top.lingkang.screen.PlayScreen;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 玩家的飞机
 */
public class PlayerSprite extends BaseSprite implements Disposable {
    private PlayScreen playScreen;
    // 生命值
    public int lifeValue;
    public PlaneType type = PlaneType.PLAYER;
    public float moveSpeed = 3f;
    private TextureAtlas atlas;
    private TextureRegion[] effect1;
    private int effectIndex = 0;
    private boolean isInvincible = true;
    private Sound sendSound;
    private Sound didiSound;
    private float bulletSpeed = 4f;
    private Sound sound1, sound2, sound3;

    public PlayerSprite(float x, float y, int lifeValue, PlayScreen playScreen) {
        this.lifeValue = lifeValue;
        this.playScreen = playScreen;
        setPosition(x, y);
        setRegion(new Texture(Gdx.files.internal("plane/player1.png")));
        setRegionWidth(32);
        setRegionHeight(32);
        // 生命
        playScreen.stage.life.setText(lifeValue);
        // 特效
        atlas = new TextureAtlas(Gdx.files.internal("effect/e1.atlas"));
        effect1 = new TextureRegion[30];
        Array<TextureAtlas.AtlasRegion> regions = atlas.getRegions();
        int tmp = -1;
        for (int i = 0; i < regions.size; i++) {
            effect1[++tmp] = atlas.getRegions().get(i);
            effect1[++tmp] = atlas.getRegions().get(i);
        }
        sendSound = playScreen.game.assetManager.get("music/send.mp3");
        didiSound = playScreen.game.assetManager.get("music/didi.mp3");
        sound1 = playScreen.game.assetManager.get("music/jiangli.mp3");
        sound2 = playScreen.game.assetManager.get("music/dingdong.mp3");
        sound3 = playScreen.game.assetManager.get("music/wanmei.mp3");

        addInvincible(4000);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
    }

    public void update(SpriteBatch batch, float delta) {
        // 绘制飞机
        batch.draw(this, getX(), getY());
        // 绘制无敌特效
        if (isInvincible) {
            batch.draw(effect1[effectIndex], getX() - 6, getY());
            if (effectIndex >= 29) {
                effectIndex = 0;
            } else {
                effectIndex++;
            }
        }
        if (attackTime < 0.3f) {
            attackTime += delta;
        }
    }

    /**
     * 中弹
     */
    public void beHit() {
        if (isInvincible) {
            return;
        }
        // 更新舞台生命
        playScreen.stage.life.setText(--lifeValue);
        didiSound.play();
        // 添加无敌
        addInvincible(4000);
    }

    // 攻击频率
    private float maxAttackTime = 0.3f;
    private float attackTime = 0.1f;

    public void attack(PlayScreen playScreen, float delta) {
        if (attackTime < maxAttackTime)
            return;
        else {
            attackTime = 0;
            sendSound.play(0.6f);
        }

        BulletSprite obtain = playScreen.bulletPool.obtain();
        obtain.init(BulletType.PLAYER, bulletSpeed, getX() + 16, getY() + 10, playScreen);
        playScreen.bulletSprites.add(obtain);

        obtain = playScreen.bulletPool.obtain();
        obtain.init(BulletType.PLAYER, bulletSpeed, getX(), getY() + 10, playScreen);
        playScreen.bulletSprites.add(obtain);
    }

    // 添加无敌
    private void addInvincible(long t) {
        isInvincible = true;
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // 取消无敌
                isInvincible = false;
                timer.cancel();
            }
        }, t);
    }

    // 移动限制在相机内
    private static final float width = MyGdxGame.VIEW_WIDTH - 32;
    private static final float height = MyGdxGame.VIEW_HEIGHT - 32;

    public void move(MoveType type) {
        switch (type) {
            case UP: {
                float move = getY() + moveSpeed;
                if (move < height)
                    setY(move);
                break;
            }
            case DOWN: {
                float move = getY() - moveSpeed;
                if (move > 0)
                    setY(move);
                break;
            }
            case LEFT: {
                float move = getX() - moveSpeed;
                if (move > 0)
                    setX(move);
                break;
            }
            case RIGHT: {
                float move = getX() + moveSpeed;
                if (move < width)
                    setX(move);
                break;
            }
        }
    }

    // 获得道具
    public void addProp(PropType type) {
        switch (type) {
            case LIFE:
                sound2.play();
                if (lifeValue < 6)
                    lifeValue++;
                playScreen.stage.life.setText(lifeValue);
                break;
            case ATTACK:
                if (maxAttackTime > 0.1) {
                    maxAttackTime -= 0.03;
                } else if (bulletSpeed < 8) {
                    bulletSpeed++;
                }
                sound1.play();
                break;
            case INVINCIBLE:
                sound3.play();
                addInvincible(9000);
                break;
        }
    }

    @Override
    public void dispose() {
        atlas.dispose();
        sendSound.dispose();
    }
}
