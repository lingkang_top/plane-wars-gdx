package top.lingkang.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import top.lingkang.constants.PropType;
import top.lingkang.screen.PlayScreen;

/**
 * 道具
 */
public class PropSprite extends BaseSprite {
    public boolean alive;
    private PlayScreen playScreen;
    private PropType propType;

    public PropSprite(PlayScreen playScreen, PropType propType, float x, float y) {
        this.playScreen = playScreen;
        this.propType = propType;
        alive = true;
        setX(x);
        setY(y);
        switch (propType) {
            case ATTACK:
                setRegion(playScreen.game.assetManager.get("tiles/t-25.png", Texture.class));
                break;
            case LIFE:
                setRegion(playScreen.game.assetManager.get("tiles/t-24.png", Texture.class));
                break;
            case INVINCIBLE:
                setRegion(playScreen.game.assetManager.get("tiles/t-26.png", Texture.class));
                break;
        }
    }

    public void update(SpriteBatch batch, float delta) {
        if (getY() < -100) {
            alive = false;
        }
        setY(getY() - playScreen.cameraMoveSpeed);
        if (this.hasOverlap(playScreen.playerPlane)) {
            alive = false;
            // 玩家吃到
            playScreen.playerPlane.addProp(propType);
        } else {
            batch.draw(this, getX(), getY());
        }
    }
}
