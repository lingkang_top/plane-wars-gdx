package top.lingkang.sprite;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Disposable;
import top.lingkang.constants.*;
import top.lingkang.screen.PlayScreen;

/**
 * @author lingkang
 * @date 2021/10/17 3:11
 * @description 敌人飞机
 */
public class EnemyPlaneSprite extends BaseSprite implements Disposable {
    private PlayScreen playScreen;
    public PlaneType type;
    public boolean alive;
    private int lifeValue = 2;
    private float existsTime = 0f;
    private float attackSpaceTime = 4f;// 随机攻击间隔
    private AttackType attackType = AttackType.NONE;
    private FlightType flightType;
    private float moveSpeed;
    private Sound soundDie;

    public EnemyPlaneSprite(PlaneType type, FlightType flightType, AttackType attackType, float moveSpeed, float x, float y, PlayScreen playScreen) {
        this.playScreen = playScreen;
        this.flightType = flightType;
        this.type = type;
        this.moveSpeed = moveSpeed;
        this.attackType = attackType;
        setPosition(x, y);
        alive = true;
        switch (type) {
            case ENEMY_5: {
                setRegion(playScreen.game.assetManager.get("plane/ship_0005.png", Texture.class));
                break;
            }
        }
        attackSpaceTime = MathUtils.random(1f, 4f);
        soundDie = playScreen.game.assetManager.get("music/pengzhuang.mp3");
    }

    public void update(SpriteBatch batch, float delta) {
        if (getY() < -100 || getY() > 800 || getX() < -100 || getX() > 550) { // 超出屏幕
            alive = false;
            return;
        }

        // 保持跟随相机移动
        setY(getY() - playScreen.cameraMoveSpeed);

        // 是否攻击
        switch (attackType) {
            case COMMON_1: {
                if (existsTime > attackSpaceTime) {
                    existsTime = 0;
                    BulletSprite obtain = playScreen.bulletPool.obtain();
                    obtain.init(BulletType.ENEMY, moveSpeed + 2, getX() + 8, getY(), playScreen);
                    playScreen.bulletSprites.add(obtain);
                }
                existsTime += delta;
                break;
            }
        }

        // 飞行方式
        switch (flightType) {
            case LEFT_TO_RIGHT:
                setX(getX() + moveSpeed);
                batch.draw(this, getX(), getY(), 16, 16, 32, 32, 1, 1, 270);
                break;
            case RIGHT_TO_LEFT:
                setX(getX() - moveSpeed);
                batch.draw(this, getX(), getY(), 16, 16, 32, 32, 1, 1, 90);
                break;
            case UP_TO_DOWN:
                setY(getY() - moveSpeed);
                batch.draw(this, getX(), getY(), 16, 16, 32, 32, 1, 1, 180);
                break;
            case NONE:
                batch.draw(this, getX(), getY(), 16, 16, 32, 32, 1, 1, 180);
        }

        // 是否碰到玩家
        if (this.hasOverlap(playScreen.playerPlane)) {
            playScreen.playerPlane.beHit();
        }

    }

    /**
     * 中弹
     */
    public void beHit() {
        lifeValue--;
        if (lifeValue == 0) {
            alive = false;
            soundDie.play();
            playScreen.stage.fenshuObservable.onNext(100);
            // 是否爆道具
            int random = MathUtils.random(1, 20);
            if (random == 5) {
                PropSprite sprite = new PropSprite(playScreen, PropType.ATTACK, getX() + 5, getY() + 5);
                playScreen.propSprites.add(sprite);
            } else if (random == 9) {
                PropSprite sprite = new PropSprite(playScreen, PropType.LIFE, getX() + 5, getY() + 5);
                playScreen.propSprites.add(sprite);
            } else if (random == 19) {
                PropSprite sprite = new PropSprite(playScreen, PropType.INVINCIBLE, getX() + 5, getY() + 5);
                playScreen.propSprites.add(sprite);
            }
        }
    }

    @Override
    public void dispose() {
    }
}
