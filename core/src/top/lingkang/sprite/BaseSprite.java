package top.lingkang.sprite;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * @author lingkang
 * @date 2021/10/17 3:13
 * @description 基础精灵 实现一些公用的方法
 */
public class BaseSprite extends Sprite {

    /**
     * 判断两个纹理是否存在重叠
     */
    public boolean hasOverlap(Sprite sprite) {
        return (getX() + getRegionWidth() > sprite.getX() &&
                sprite.getX() + sprite.getRegionWidth() > getX() &&
                getY() + getRegionHeight() > sprite.getY() &&
                sprite.getY() + sprite.getRegionHeight() > getY());
    }
}
