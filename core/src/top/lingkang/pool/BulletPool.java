package top.lingkang.pool;

import com.badlogic.gdx.utils.Pool;
import top.lingkang.constants.BulletType;

/**
 * https://hub.fastgit.org/libgdx/libgdx/wiki/Memory-management
 */
public class BulletPool implements Pool.Poolable {
    public float x, y;
    public BulletType type;
    public boolean alive;
    private float moveSpeed;

    public void init(float x, float y, BulletType type, float moveSpeed) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.moveSpeed = moveSpeed;
        this.alive = true;
    }

    public void update(float delta) {
        //x=x+delta*moveSpeed;
        y = y + delta * moveSpeed;
        if (y > 800) { // 超出屏幕
            alive = false;
        }
    }

    @Override
    public void reset() {
        alive = false;
    }
}
