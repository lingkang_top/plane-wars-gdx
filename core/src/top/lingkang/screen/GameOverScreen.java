package top.lingkang.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import top.lingkang.MyGdxGame;


public class GameOverScreen extends GameScreen {

    private MyGdxGame game;

    public GameOverScreen(final MyGdxGame game) {
        this.game = game;
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        /*if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
            System.out.println("game over!");
            Gdx.app.log("GameOverScreen", "game over!");
            // 进入玩游戏场景
            game.setScreen(new PlayScreen(game));
            dispose();
        }*/
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
