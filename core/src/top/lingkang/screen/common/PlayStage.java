package top.lingkang.screen.common;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class PlayStage extends Stage {
    private Table table;
    private Label fenshu;
    public Label life, distance;
    private int fs = 0;
    public Subject<Integer> fenshuObservable = PublishSubject.create();

    public PlayStage(Viewport viewport) {
        super(viewport);
        // libgdx 默认的样式是不支持中文的，要自己去制作样式的，可以参考前面的文章
        Label.LabelStyle labelStyle = new Label.LabelStyle(new BitmapFont(), Color.RED);
        table = new Table();
        table.top();
        table.setFillParent(true);
        table.add(new Label("life", labelStyle)).expandX().padTop(10);
        table.add(new Label("fenshu", labelStyle)).expandX().padTop(10);
        table.add(new Label("distance", labelStyle)).expandX().padTop(10);
        table.row();// 添加新行
        table.add(life = new Label("0", labelStyle)).expandX();
        table.add(fenshu = new Label("0", labelStyle)).expandX();
        table.add(distance = new Label("0", labelStyle)).expandX();
        addActor(table);

        fenshuObservable.subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                fenshu.setText(fs += integer);
            }
        });
    }
}
