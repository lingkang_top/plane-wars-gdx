package top.lingkang.constants;

public enum FlightType {
    NONE,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    UP_TO_DOWN,
}
