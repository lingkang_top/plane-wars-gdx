package top.lingkang.constants;

/**
 * @author lingkang
 * @date 2021/10/16 15:21
 * @description
 */
public enum MoveType {
    LEFT,RIGHT,UP,DOWN
}
