package top.lingkang;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import top.lingkang.screen.WelcomeScreen;

public class MyGdxGame extends Game {
    // 用于屏幕自适应的长宽
    public static final int VIEW_WIDTH = 480;
    public static final int VIEW_HEIGHT = 648;
    // 画面自适应
    public Viewport viewport;
    // 类似 javafx 的 Stage 我们绘制的画面在它上面
    public Stage stage;
    public BitmapFont bitmapFont;
    public SpriteBatch batch;
    public OrthographicCamera camera;
    // 游戏资源管理
    public AssetManager assetManager;

    @Override
    public void create() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(MyGdxGame.VIEW_WIDTH, MyGdxGame.VIEW_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, batch);
        bitmapFont = new BitmapFont();
        assetManager = new AssetManager();
        // 进入欢迎页面
        setScreen(new WelcomeScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        stage.dispose();
        bitmapFont.dispose();
        assetManager.clear();
        assetManager.dispose();
    }
}
