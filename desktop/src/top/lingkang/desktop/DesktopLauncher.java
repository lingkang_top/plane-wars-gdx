package top.lingkang.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import top.lingkang.MyGdxGame;

/**
 * @author lingkang
 */
public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        // 窗口宽高
        config.width = 600;
        config.height = 800;
        // 窗口显示的位置
        config.x = 0;
        config.y = 0;
        // 标题图标
        config.title = "无尽-飞机大战";
        config.useGL30 = true;
        config.addIcon("plane/player1.png", Files.FileType.Internal);
        new LwjglApplication(new MyGdxGame(), config);
    }
}
