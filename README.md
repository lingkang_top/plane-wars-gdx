# 无尽飞机大战
这是一个基于libgdx开发的java游戏，jdk8+
## 1、打jar包
```shell
gradlew desktop:dist -Dfile.encoding=utf-8
```
## 2、运行
```shell
java -jar -Dfile.encoding=utf-8 -Xms256m -Xmx512m desktop-1.0.jar
```
![运行2](https://gitee.com/lingkang_top/plane-wars-gdx/raw/master/picture/2.png)
## 3、打exe在window上运行
打包exe方式请参考我的文章：
https://blog.csdn.net/weixin_44480167/article/details/120895260
![运行1](https://gitee.com/lingkang_top/plane-wars-gdx/raw/master/picture/1.png)
## 架构
> libgdx + gradle